package com.example.ayumudemo

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_form.*
import kotlinx.android.synthetic.main.activity_picker.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Period
import java.time.format.DateTimeFormatter
import java.util.*

@Suppress("ASSIGNED_BUT_NEVER_ACCESSED_VARIABLE")
class FormActivity : AppCompatActivity() {

    private val spinnerYearItems = arrayListOf("")
    private val spinnerMonthItems = arrayListOf("")
    private val spinnerDayItems = arrayListOf("")


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.title = "アンケート"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//
//        fun getday():String {
//            val date = Date()
//            val format = SimpleDateFormat("yyyy", Locale.getDefault())
//            return format.format(date)
//        }
//        fun getMonth():String{
//            val date = Date()
//            val format = SimpleDateFormat("MM",Locale.getDefault())
//            return format.format(date)
//        }
//        fun getDay():String{
//            val date = Date()
//            val format = SimpleDateFormat("dd",Locale.getDefault())
//            return format.format(date)
//        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)

        val adapter = ArrayAdapter(
            applicationContext,
            android.R.layout.simple_spinner_item, spinnerYearItems
        )

        val adapter2 = ArrayAdapter(
            applicationContext,
            android.R.layout.simple_spinner_item, spinnerMonthItems
        )

        val adapter3 = ArrayAdapter(
            applicationContext,
            android.R.layout.simple_spinner_item, spinnerDayItems
        )


//        val adapter = ArrayAdapter(applicationContext,android.R.layout.simple_spinner_item,spinnerYearItems)
//        val adapter2 = ArrayAdapter(applicationContext,android.R.layout.simple_spinner_item,spinnerMonthItems)
//        val adapter3 = ArrayAdapter(applicationContext,android.R.layout.simple_spinner_item,spinnerDayItems)
//
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)


        spinner_year.adapter = adapter
        spinner_month.adapter = adapter2
        spinner_day.adapter = adapter3


        var year: String? = null
        var month: String? = null
        var day: String? = null
        var radio: String? = null
        var check1_text: String? = null
        var check2_text: String? = null
        var check3_text: String? = null
        var check4_text: String? = null
        var check5_text: String? = null

//        var d: String? = null

//
//        val s = LocalDate.now()
//
//        fun calcAge(birthday: LocalDate): Int? {
//            val today = LocalDate.now()
//            return Period.between(LocalDate.parse(birthday.toString()), today).getYears()
//        }


        //スピナーの値をゲットお

        spinner_year.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val spy = p0 as Spinner
                year = spy.selectedItem as String
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }
        spinner_month.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val spm = p0 as Spinner
                month = spm.selectedItem as String
//                spi = Integer.parseInt(month!!)
//                A = getMonth()
//                a = Integer.parseInt(A)
//                month = spm.selectedItem.toString()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }
        spinner_day.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val spd = p0 as Spinner
                day = spd.selectedItem as String
//                d = (year.toString() + month.toString() + day.toString())
//                val formatter = DateTimeFormatter.ofPattern("yyyyMd")
//                var date = LocalDate.parse(d,formatter)
//                date.toString().format(DateTimeFormatter.ofPattern("yyyyMMdd"))
//                val age = Period.between(LocalDate.of(year!!.toInt(),month!!.toInt(),day!!.toInt()), s).years.toString()
//                val aaa = date.format
//                textView30.setText(age)


            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }


        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            radio = findViewById<RadioButton>(checkedId).text as String
        }


        val checkBox1: CheckBox = findViewById(R.id.checkBox)
        checkBox1.setOnCheckedChangeListener { _, isChecked ->
            check1_text = if (isChecked) "インターネット" else ""
        }
        val checkBox2: CheckBox = findViewById(R.id.checkBox2)
        checkBox2.setOnCheckedChangeListener { _, isChecked ->
            check2_text = if (isChecked) "雑誌記事" else ""
        }
        val checkBox3: CheckBox = findViewById(R.id.checkBox3)
        checkBox3.setOnCheckedChangeListener { _, isChecked ->
            check3_text = if (isChecked) "友人・知人から" else ""
        }
        val checkBox4: CheckBox = findViewById(R.id.checkBox4)
        checkBox4.setOnCheckedChangeListener { _, isChecked ->
            check4_text = if (isChecked) "セミナー" else ""
        }
        val checkBox5: CheckBox = findViewById(R.id.checkBox5)
        checkBox5.setOnCheckedChangeListener { _, isChecked ->
            check5_text = if (isChecked) "その他" else ""
        }


        button.setOnClickListener { //3.Intentクラスのオブジェクトを生成。
            var name1 = userName_edit.text.toString()
            var name2 = nenrei.text.toString()
            val intent = Intent(this@FormActivity, FormActivity2::class.java)
            //生成したオブジェクトを引数に画面を起動！

            intent.putExtra("USER_NAME", name1)
            intent.putExtra("RADIO", radio)
            intent.putExtra("YEAR", year)
            intent.putExtra("MONTH", month)
            intent.putExtra("DAY", day)
            intent.putExtra("NENREI", name2)
            intent.putExtra("CHECK", check1_text)
            intent.putExtra("CHECK2", check2_text)
            intent.putExtra("CHECK3", check3_text)
            intent.putExtra("CHECK4", check4_text)
            intent.putExtra("CHECK5", check5_text)
            startActivity(intent)

        }
        for (i in 1900..2020) {
            spinnerYearItems.add("$i")
        }

        for (i in 1..12) {
            spinnerMonthItems.add("$i")
        }
        for (i in 1..31) {
            spinnerDayItems.add("$i")
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}



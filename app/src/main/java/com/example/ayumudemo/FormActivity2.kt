package com.example.ayumudemo

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_form.*
import kotlinx.android.synthetic.main.activity_form2.*


class FormActivity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form2)

        var name1 = intent.getStringExtra("USER_NAME")

        var radio = intent.getStringExtra("RADIO")

        var year = intent.getStringExtra("YEAR")

        var month = intent.getStringExtra("MONTH")


        var day = intent.getStringExtra("DAY")

        var nenrei = intent.getStringExtra("NENREI")


        var check1_text = intent.getStringExtra("CHECK")
        var check2_text = intent.getStringExtra("CHECK2")
        var check3_text = intent.getStringExtra("CHECK3")
        var check4_text = intent.getStringExtra("CHECK4")
        var check5_text = intent.getStringExtra("CHECK5")


        name_text.setText(name1)

        radio_text.setText(radio)

        year_text.setText(year)

        month_text.setText(month)

        day_text.setText(day)

        nenrei1.setText(nenrei)

        internet.setText(check1_text)
        zassikij.setText(check2_text)
        yuujin.setText(check3_text)
        semina.setText(check4_text)
        sonota.setText(check5_text)

        button_top.setOnClickListener {
            val intent=Intent(this@FormActivity2, MainActivity::class.java)
            startActivity(intent)

        }

    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}